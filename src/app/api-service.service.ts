import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class ApiServiceService {

  protected handleError(error: HttpErrorResponse) {
    console.log("ABSTRACT CLASS");
    if(error.status === 500) {
        console.log("Not Found")
    }
    return throwError({
        error,
        redirection: true,
        message: "Please Try Again"
    });
}
}

