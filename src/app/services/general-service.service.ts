import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs';
import { ApiServiceService } from '../api-service.service';

@Injectable({
  providedIn: 'root'
})
export class GeneralServiceService extends ApiServiceService{

  constructor(private http: HttpClient) {
    super();
  }

 
  CommonApi(ApiUrl: string, PostData: any, MethodType: string = "post") {
    switch (MethodType) {
      case "post":
        return this.http.post('http://127.0.0.1:8000/api/'+ ApiUrl, {
          ...PostData
        }).pipe(catchError(this.handleError));

      // case "get":
      //   return this.http.get('http://127.0.0.1:8000/api/' + ApiUrl, {
      //     params: PostData


      //   }).pipe(catchError(this.handleError));

      // case "put":
      //   return this.http.put('http://127.0.0.1:8000/api/' + ApiUrl, {
      //     ...PostData
      //   }).pipe(catchError(this.handleError));

      // case "delete":
      //   return this.http.delete('http://127.0.0.1:8000/api/' + ApiUrl, {
      //     params: PostData


      //   }).pipe(catchError(this.handleError));

      default:
        return this.http.get('url' + ApiUrl, {
          ...PostData
        }).pipe(catchError(this.handleError));

    }




  }


}
