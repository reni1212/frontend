import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "reservation", loadChildren: () => import("./reservation/reservation.module").then(m => m.ReservationModule),data:{title:"Reservation Form UI",route_name:"reservation"}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
