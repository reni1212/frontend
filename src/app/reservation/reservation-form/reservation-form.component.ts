import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Data } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { GeneralServiceService } from '../../services/general-service.service';

@Component({
  selector: 'app-reservation-form',
  templateUrl: './reservation-form.component.html',
  styleUrls: ['./reservation-form.component.scss']
})
export class ReservationFormComponent implements OnInit {

model: any;
userId: any;
myDatePicker:any;
submitted: boolean = false;
reactiveForm:FormGroup;
mnth:any;
day:any;
hours:any;
minutes:any;
seconds:any;
finalDate:any;
finalTime:any;
dateTimeval:any;
userIdVal:any = [];
user_ids:any;
reservation_datetime:any;
resultData:any;
public error:any=[];

  constructor(private formBuilder: FormBuilder, private service: GeneralServiceService) { 

    this.reactiveForm = this.formBuilder.group({
      userIdVal: new FormControl(null, [Validators.required,Validators.pattern("^[-,0-9]+$")]),
      dateVal: new FormControl(null,[Validators.required])
      });
  }

  get f() {
    return this.reactiveForm.controls;
  }


  ngOnInit(): void {
  }

  onSubmit(){
    this.submitted = true;

    if(this.reactiveForm.controls['userIdVal'].status == "VALID" && this.reactiveForm.controls['dateVal'].status == "VALID"){
      this.userIdVal = Array(this.reactiveForm.controls['userIdVal'].value) ;

      var date = new Date(this.reactiveForm.controls['dateVal'].value);
      this.mnth = ("0" + (date.getMonth() + 1)).slice(-2);
      this.day = ("0" + date.getDate()).slice(-2);
      this.hours  = ("0" + date.getHours()).slice(-2);
      this.minutes = ("0" + date.getMinutes()).slice(-2);
      this.seconds = ("0" + date.getSeconds()).slice(-2);

      this.finalDate = [date.getFullYear(), this.mnth, this.day].join("-");
      this.finalTime = [this.hours, this.minutes, this.seconds].join(":");
      this.dateTimeval = Array([this.finalDate, this.finalTime].join(" "));

      const data: Data={ data: {
          user_ids: this.userIdVal,
          reservation_datetime: this.dateTimeval
        }
      }
      
      this.service.CommonApi('add-reservation' , data, "post").subscribe((response) => {
      
        this.resultData = response;
        console.log(this.resultData);
      }, (error) => {
        console.log("Error", error);
      });

    }else{
      console.log('Please select the fields!');
    }

  }
  
}
